## jcodec - a pure java implementation of video/audio codecs.

## Introduction 

JCodec is a library implementing a set of popular video and audio codecs.

Currently JCodec supports MP4 Video format.

JCodec is free software distributed under FreeBSD License.

### Features supported:
1) Decode a particular Image frame from a video file.
2) Performance / quality considerations
Because JCodec is a pure Java implementation please adjust your performance expectations accordingly. We usually make the best effort to write efficient code but despite this the decoding will typically be an order of magnitude slower than the native implementations (such as FFMpeg). We are currently looking into implementing performance-critical parts in OpenCL but the ETA is unknown.

Expect the encoded quality/bitrate for h.264 (AVC) to be so much worse compared to the well known native encoders (such as x264). This is because very little work has been put so far into developing the encoder and also because encoders usually trade speed for quality, speed is something we don't have in Java, hence the quality. Again we may potentially fix that in the future by introducing OpenCL (RenderScript) code but at this point it's an unknown.

That said the decode quality should be at the industry level. This is because the decoding process is usually specified by the standard and the correct decoder implementations are expected to produce bit-exact outputs.

## Usage Instruction

### Getting a single frame from a movie ( supports only AVC, H.264 in MP4, ISO BMF, Quicktime container ):

```java
int frameNumber = 42;
Picture picture = FrameGrab.getFrameFromFile(new File("/data/user/0/org.jcodec.samples/cache/jcodec_cache/sample.mp4"), frameNumber);

//for openharmony (jcodec-openharmony)
PixelMap bitmap = HarmonyUtil.toBitmap(picture);
IMAGEVIEW.setPixelMap( bitmap);

```

### Read Tape Timecode from MXF file

```java
TapeTimecode timecode = MXFDemuxer.readTapeTimecode(new File("myfile.mxf"));
```

### Parse DPX metadata

```java
DPXMetadata dpx = DPXReader.readFile(firstDpx).parseMetadata();
System.out.println(dpx.getTimecodeString());
```

### Parse Apple GPS metadata from MOV or MP4 file
```java
MovieBox moov = MP4Util.parseMovie(new File("gps1.mp4"));
UdtaBox udta = NodeBox.findFirst(moov, UdtaBox.class, "udta");
String latlng = udta.latlng();
assertEquals("-35.2840+149.1215/", latlng);
```

OR

```java
MovieBox mov = MP4Util.parseMovie(new File("gps2.MOV"));
Box found = findDeep(mov, "meta");
if (found == null) {
    System.out.println("Atom " + atom + " not found.");
    return null;
} else {
    System.out.println("Atom found " + found.toString());
}

```

### Extract subtitles from MKV file
```java
MKVDemuxer demuxer = new MKVDemuxer(new AutoFileChannelWrapper(new File("subs.mkv")));
DemuxerTrack track = demuxer.getSubtitleTracks().get(0);
Packet packet;
while (null != (packet = track.nextFrame())) {
    String text = takeString(packet.getData());
    System.out.println("time: " + packet.pts + " text: " + text);
}
```

### MP4/M4A/MOV metadata versions

Some editors (e.g. Final Cut Pro 7) employ a clever hack to support multiple versions of metadata for mp4 files.
The trick is to append a new version of `moov` atom to end of file and set previous version atom name to `free`.
Jcodec supports this hack via `org.jcodec.movtool.MoovVersions` 

See `MoovVersionsTest.java` for complete usage example.

To list available versions use

```java
MoovVersions.listMoovVersionAtoms(new File("/data/my.mp4"))
```

To add a version

```java
moov = MP4Util.parseMovie(new File("/data/my.mp4"))
//add your modifications to moov here
MoovVersions.addVersion(new File("/data/my.mp4"), moov)
```

To rollback (undo) to previous version

```java
MoovVersions.undo(new File("/data/my.mp4"))
```

To rollback to specific  version

```java
versions = MoovVersions.listMoovVersionAtoms(new File("my.mp4"))
//pick your version
version = versions.get(Math.random()*versions.size())
MoovVersions.rollback(new File("my.mp4"), version)
```

## Installation instruction
JCodec can be used in both standard Java and openharmony. It contains platform-agnostic java classes.
### Method 1:
1. Add the .har package to the lib folder.
2. Add the following code to the gradle of the entry:
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

### Method 2:

```gradle
allprojects {
    repositories {
        mavenCentral()
    }
}

dependencies {
    implementation 'io.openharmony.tpc.thirdlib:jcodec:1.0.2'
    implementation 'io.openharmony.tpc.thirdlib:harmony:1.0.0'
}
```

## License

    Copyright 2008-2019 JCodecProject

    Redistribution  and  use  in   source  and   binary   forms,  with  or  without
    modification, are permitted provided  that the following  conditions  are  met:

    Redistributions of  source code  must  retain the above  copyright notice, this
    list of conditions and the following disclaimer. Redistributions in binary form
    must  reproduce  the above  copyright notice, this  list of conditions  and the
    following disclaimer in the documentation and/or other  materials provided with
    the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO, THE  IMPLIED
    WARRANTIES  OF  MERCHANTABILITY  AND  FITNESS  FOR  A  PARTICULAR  PURPOSE  ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,  OR CONSEQUENTIAL DAMAGES
    (INCLUDING,  BUT NOT LIMITED TO,  PROCUREMENT OF SUBSTITUTE GOODS  OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS;  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    ANY  THEORY  OF  LIABILITY,  WHETHER  IN  CONTRACT,  STRICT LIABILITY,  OR TORT
    (INCLUDING  NEGLIGENCE OR OTHERWISE)  ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.