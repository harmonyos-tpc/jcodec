package org.jcodec.api.harmony;

import ohos.media.image.PixelMap;

/**
 * This class is part of JCodec ( www.jcodec.org ) This software is distributed
 * under FreeBSD License
 * 
 * @author The JCodec project
 * 
 */
public class BitmapWithMetadata {
    private PixelMap bitmap;
    private double timestamp;
    private double duration;

    public BitmapWithMetadata(PixelMap bitmap, double pts, double duration) {
        this.bitmap = bitmap;
        this.timestamp = pts;
        this.duration = duration;
    }

    public PixelMap getBitmap() {
        return bitmap;
    }

    public double getTimestamp() {
        return timestamp;
    }
    
    public double getDuration() {
        return duration;
    }
}
