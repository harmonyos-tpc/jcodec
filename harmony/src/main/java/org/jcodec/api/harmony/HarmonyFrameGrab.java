package org.jcodec.api.harmony;

import java.io.File;
import java.io.IOException;

import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.api.PictureWithMetadata;
import org.jcodec.api.UnsupportedFormatException;
import org.jcodec.api.specific.ContainerAdaptor;
import org.jcodec.common.HarmonyUtil;
import org.jcodec.common.io.FileChannelWrapper;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.io.SeekableByteChannel;
import org.jcodec.common.SeekableDemuxerTrack;
import org.jcodec.common.model.Picture;

import ohos.media.image.PixelMap;

/**
 * This class is part of JCodec ( www.jcodec.org ) This software is distributed
 * under FreeBSD License
 * 
 * Extracts frames from a movie into uncompressed images suitable for
 * processing.
 * 
 * Supports going to random points inside of a movie ( seeking ) by frame number
 * of by second.
 * 
 * NOTE: Supports only AVC ( H.264 ) in MP4 ( ISO BMF, QuickTime ) at this
 * point.
 * 
 * NOTE: openharmony specific routines
 * 
 * @author The JCodec project
 * 
 */
public class HarmonyFrameGrab extends FrameGrab {

    public static HarmonyFrameGrab createHarmonyFrameGrab(SeekableByteChannel in)
            throws IOException, JCodecException {
        FrameGrab frameGrab = createFrameGrab(in);
        return new HarmonyFrameGrab(frameGrab.getVideoTrack(), frameGrab.getDecoder());
    }

    public HarmonyFrameGrab(SeekableDemuxerTrack videoTrack, ContainerAdaptor decoder) {
        super(videoTrack, decoder);
    }

    /**
     * Get frame at a specified second as AWT image
     * 
     * @param file
     * @param second
     * @return A decoded frame from a given point in video.
     * @throws IOException
     * @throws JCodecException
     */
    public static PixelMap getFrame(File file, double second) throws IOException, JCodecException {
        FileChannelWrapper ch = null;
        try {
            ch = NIOUtils.readableChannel(file);
            return ((HarmonyFrameGrab) createHarmonyFrameGrab(ch).seekToSecondPrecise(second)).getFrame();
        } finally {
            NIOUtils.closeQuietly(ch);
        }
    }

    /**
     * Get frame at a specified second as AWT image
     * 
     * @param file
     * @param second
     * @return A decoded frame from a given point in video.
     * @throws UnsupportedFormatException
     * @throws IOException
     */
    public static PixelMap getFrame(SeekableByteChannel file, double second) throws JCodecException, IOException {
        return ((HarmonyFrameGrab) createHarmonyFrameGrab(file).seekToSecondPrecise(second)).getFrame();
    }

    /**
     * Get frame at current position in AWT image
     * 
     * @return A decoded frame with metadata.
     * @throws IOException
     */
    public BitmapWithMetadata getFrameWithMetadata() throws IOException {
        PictureWithMetadata pictureWithMeta = getNativeFrameWithMetadata();
        if (pictureWithMeta == null)
            return null;
        PixelMap bitmap = HarmonyUtil.toBitmap(pictureWithMeta.getPicture());
        return new BitmapWithMetadata(bitmap, pictureWithMeta.getTimestamp(), pictureWithMeta.getDuration());
    }

    /**
     * Get frame at current position in AWT image
     * 
     * @return A decoded frame.
     * @throws IOException
     */
    public PixelMap getFrame() throws IOException {
        return HarmonyUtil.toBitmap(getNativeFrame());
    }

    /**
     * Get frame at current position in AWT image
     *
     * @param bmp
     * @throws IOException
     */
    public void getFrame(PixelMap bmp) throws IOException {
        Picture picture = getNativeFrame();
        HarmonyUtil.toBitmap(picture, bmp);
    }

    /**
     * Get frame at current position in AWT image
     * 
     * @return A decoded picture with metadata. A bitmap provided is used.
     *
     * @param bmp
     * @throws IOException
     */
    public BitmapWithMetadata getFrameWithMetadata(PixelMap bmp) throws IOException {
        PictureWithMetadata pictureWithMetadata = getNativeFrameWithMetadata();
        if (pictureWithMetadata == null)
            return null;
        HarmonyUtil.toBitmap(pictureWithMetadata.getPicture(), bmp);
        return new BitmapWithMetadata(bmp, pictureWithMetadata.getTimestamp(), pictureWithMetadata.getDuration());
    }

    /**
     * Get frame at a specified frame number as AWT image
     * 
     * @param file
     * @param frameNumber
     * @return PixelMap
     * @throws IOException
     * @throws JCodecException
     */
    public static PixelMap getFrame(File file, int frameNumber) throws IOException, JCodecException {
        FileChannelWrapper ch = null;
        try {
            ch = NIOUtils.readableChannel(file);
            return ((HarmonyFrameGrab) createHarmonyFrameGrab(ch).seekToFramePrecise(frameNumber)).getFrame();
        } finally {
            NIOUtils.closeQuietly(ch);
        }
    }

    /**
     * Get frame at a specified frame number as AWT image
     * 
     * @param file
     * @param frameNumber
     * @return PixelMap
     * @throws IOException
     * @throws JCodecException
     */
    public static PixelMap getFrame(SeekableByteChannel file, int frameNumber) throws JCodecException, IOException {
        return ((HarmonyFrameGrab) createHarmonyFrameGrab(file).seekToFramePrecise(frameNumber)).getFrame();
    }

    /**
     * Get a specified frame by number from an already open demuxer track
     * 
     * @param vt
     * @param decoder
     * @param frameNumber
     * @return PixelMap
     * @throws IOException
     * @throws JCodecException
     */
    public static PixelMap getFrame(SeekableDemuxerTrack vt, ContainerAdaptor decoder, int frameNumber)
            throws IOException, JCodecException {
        return ((HarmonyFrameGrab) new HarmonyFrameGrab(vt, decoder).seekToFramePrecise(frameNumber)).getFrame();
    }

    /**
     * Get a specified frame by second from an already open demuxer track
     * 
     * @param vt
     * @param decoder
     * @param second
     * @return PixelMap
     * @throws IOException
     * @throws JCodecException
     */
    public static PixelMap getFrame(SeekableDemuxerTrack vt, ContainerAdaptor decoder, double second)
            throws IOException, JCodecException {
        return ((HarmonyFrameGrab) new HarmonyFrameGrab(vt, decoder).seekToSecondPrecise(second)).getFrame();
    }

    /**
     * Get a specified frame by number from an already open demuxer track (
     * sloppy mode, i.e. nearest keyframe )
     * 
     * @param vt
     * @param decoder
     * @param frameNumber
     * @return PixelMap
     * @throws IOException
     * @throws JCodecException
     */
    public static PixelMap getFrameSloppy(SeekableDemuxerTrack vt, ContainerAdaptor decoder, int frameNumber)
            throws IOException, JCodecException {
        return ((HarmonyFrameGrab) new HarmonyFrameGrab(vt, decoder).seekToFrameSloppy(frameNumber)).getFrame();
    }

    /**
     * Get a specified frame by second from an already open demuxer track (
     * sloppy mode, i.e. nearest keyframe )
     * 
     * @param vt
     * @param decoder
     * @param second
     * @return PixelMap
     * @throws IOException
     * @throws JCodecException
     */
    public static PixelMap getFrameSloppy(SeekableDemuxerTrack vt, ContainerAdaptor decoder, double second)
            throws IOException, JCodecException {
        return ((HarmonyFrameGrab) new HarmonyFrameGrab(vt, decoder).seekToSecondSloppy(second)).getFrame();
    }
}